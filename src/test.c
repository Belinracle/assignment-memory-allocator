#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#include "util.h"

#define TEST1_CAPACITY 10000

struct block_header* start_test(int capacity){
    printf("Start test\n");
    printf("Initializing heap with size %d\n", capacity);
    return (struct block_header*) heap_init(capacity);
}

void end_test(struct block_header* heap, size_t heap_size){
    munmap(heap,heap_size);
    printf("End test\n");
}

void test_failed(char* reason){
    err("Test Failed: %s\n", reason);
}

void test_successes(){
    printf("Test Successes \n");
}

void test1(){
    struct block_header* block = start_test(TEST1_CAPACITY);
    debug_heap(stdout, block);
    void* first_malloced_block = _malloc(10000);
    debug_heap(stdout, block);
    _malloc(10000);
    debug_heap(stdout, block);
    _free(first_malloced_block);
    debug_heap(stdout, block);
    if(!block->is_free){
        test_failed("first_malloced and freed block is not free");
    }
    test_successes();
    end_test(block,TEST1_CAPACITY);
}
